package com.example.scenario.persistencia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.example.scenario.ListArticulos;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DbProductos extends DbHelper{

    Context context; // Variable global
    FirebaseFirestore firebaseFirestore;
    FirebaseFirestore dbf = firebaseFirestore.getInstance();


    // ESTOY INTENTANDO OBTENER UN ID QUE CAPTURA CUANDO CREA UN NUEVO PRODUCTO
    //getSnapshots().getSnapshot(holder.getAdapterPosition());
    DocumentSnapshot articuloDocument = getSnapshots().getSnapshot(position).getId();
    final String id = articuloDocument.getId();

    // Constructor
    public DbProductos(@Nullable Context context){
        super(context);
        this.context = context;

    }

    // NO ESTOY SEGURA SI AGREGAR UN ARTICULO, SE HACE DE ESTA FORMA (ESTÁ HECHO CON FRAGMENTOS (CARDS))
    public void agregarArticulo(String nombrep, String descripcion, String precio) {
        // ContentValues cv = new ContentValues(); // Instancia
        Map<String, Object> cv = new HashMap<>(); // Instancia
        cv.put("nombrep", nombrep);
        cv.put("descripcion", descripcion);
        cv.put("precio", precio);
        // this.getWritableDatabase().insert("productos", null, cv);
        firebaseFirestore.collection("productos").add(cv);
    }
    // NO ESTOY SEGURA SI ELIMINAR UN ARTICULO SE HACE DE ESTA FORMA
    public void eliminarArticulo(@NonNull View view) {
        firebaseFirestore.collection("productos").document(id).delete();
        //this.getWritableDatabase().delete("productos", "codigop = ?", new String[]{codigop.trim()});
    }

    // NO ESTOY SEGURA SI ACTUALIZAR UN ARTICULO SE HACE DE ESTA FORMA
    public void actualizarArticulo(String descripcion, String precio) {
        // ContentValues cv = new ContentValues(); // Instancia
        Map<String, Object> cv = new HashMap<>();
        cv.put("descripcion", descripcion);
        cv.put("precio", precio);
        firebaseFirestore.collection("productos").document(id).update(cv);

        // trim = Es un método que se encarga de eliminar caracteres blancos iniciales y finales de una cadena de texto (String)
        //this.getWritableDatabase().update("productos", cv, "codigop = ?", new String[]{codigop.trim()});
    }

    // PARA CONSULTAR UN ARTICULO TENGO NULO CONOCIMIENTO PARA HACERLO CON FIRESTORE
    public List<ListArticulos> consultarArticulos() {
        List<ListArticulos> listArticulos = new ArrayList<ListArticulos>(); // Instancia de un objeto tipo lista
        // Cursor: sirve para  buscar datos por medio de saltos en las columna existentes en la base de datos
        Cursor result = this.getWritableDatabase().query("productos", new String[]{"codigop", "nombrep", "descripcion", "precio"}, null, null, null, null, null);



        Query query = dbf.collection("productos");

        while (result.moveToNext()) {
            ListArticulos nuevoArticulo = new ListArticulos(
                    result.getInt((int) result.getColumnIndex("codigop")),
                    result.getString((int) result.getColumnIndex("nombrep")),
                    result.getString((int) result.getColumnIndex("descripcion")),
                    result.getFloat((int) result.getColumnIndex("precio"))
            );
            listArticulos.add(nuevoArticulo);
        }

        return listArticulos;
    }


}
